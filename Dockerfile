FROM alpine

RUN apk add --update --no-cache \
    jq yq git sed curl && \
    rm -rf /var/cache/apt/*